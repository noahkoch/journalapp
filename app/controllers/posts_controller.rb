class PostsController < ApplicationController
  def index
    @posts = Post.all
    respond_to do |format|
      format.json {render json: @posts}
      format.html {render :index}
    end
  end

  def show
    @post = Post.find(params[:id])
    respond_to do |format|
      # format.html {render show: @post}
      format.json {render json: @post}
    end
  end

  def destroy
    @post = Post.find(params[:id])
    @post.destroy
    respond_to do |format|
      format.json {render json: :index}
      format.html {render :index}
    end
  end

  def create
    @post = Post.new(params[:post])
    @post.save
    respond_to do |format|
      format.json {render json: @post}
    end
  end
end
