JR.Collections.Posts = Backbone.Collection.extend({
  model: JR.Models.Post,
  url: '/posts'
});