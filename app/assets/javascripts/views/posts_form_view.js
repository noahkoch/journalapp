JR.Views.PostsFormView = Backbone.View.extend({
  events: {
    "click button.submit": "postAction"
  },

  render: function(){
    var formTemplate = JST["posts/form"]();
    this.$el.html(formTemplate);
    return this
  },

  postAction: function(event){
    event.preventDefault();
    var that = this;

    this.collection.create({post: {title: that.$("#title").val(),
      body: that.$("#body").val()}});

    Backbone.history.navigate("#/");
  }
});