JR.Views.PostsIndex = Backbone.View.extend({
  initialize: function(){
    var that = this;
    that.listenTo(that.collection, "add", that.render);
    that.listenTo(that.collection, "change", that.render);
    that.listenTo(that.collection, "remove", that.render);
  },

  events: {
    "click button.delete": "deletePost"
  },

  render: function(){
    var that = this;
    var ul = $('<ul></ul>');
    (that.collection).each( function(post){
      var tempId = post.escape("id")
      ul.append(
        $("<li><a href='#/posts/"+ tempId + "'>" + post.escape("title") +
        "</a><button class='delete' data-id='" + tempId +
         "'> DELETE</button></li>")
      )
    });
    ul.append($("<br><br><a href='#/posts/new'>New Post</a>"));

    that.$el.html(ul);
    return that;
  },
  deletePost: function(event){
    event.preventDefault();
    var obj = this.collection.get($(event.target).attr("data-id"))
    obj.destroy();
    this.collection.remove(obj)
  }
});