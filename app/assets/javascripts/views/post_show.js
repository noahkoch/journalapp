JR.Views.PostsShow = Backbone.View.extend({

  render: function(){
    console.log(this);
    var header = this.model.attributes.title + "\n" + this.model.attributes.body;
    var back_button = '<br><br><a href="#/">Back!</a>';
    this.$el.html(header + back_button);
    return this;
  }
});