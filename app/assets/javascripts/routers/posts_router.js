JR.Routers.Posts = Backbone.Router.extend({
  initialize: function($content, posts){
    this.$content = $content;
    this.posts = posts;
  },

  routes: {
    "": "index",
    "posts/new": "new",
    "posts/:id" : "show"

  },

  index: function(){
  },

  show: function(id){
    var that = this;
    var post = this.posts._byId[id];
    var postShow = new JR.Views.PostsShow({
      model: post
    });


    that.$content.html(postShow.render().$el);
  },

  new: function(){
    var that = this;
    var postObj = new JR.Models.Post;

    var postNew = new JR.Views.PostsFormView({collection: that.posts});
    that.$content.html(postNew.render().$el)
  }

});