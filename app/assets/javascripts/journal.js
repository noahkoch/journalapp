window.JR = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},
  initialize: function($sidebar, $content, postsData){
    var that = this;

    var posts = new JR.Collections.Posts(postsData);

    var postIndex = new JR.Views.PostsIndex({
      collection: posts
    });
    console.log(postIndex)
    $sidebar.html(postIndex.render().$el);

    new JR.Routers.Posts($content, posts);

    Backbone.history.start();
  }
};